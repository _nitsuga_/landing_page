import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.scss']
})
export class SocialComponent implements OnInit {
  year: any = new Date().getFullYear();
  socials: any[] =  [];
  constructor() { }

  ngOnInit() {
    this.socials.push({class:'fa fa-facebook', url: 'https://www.facebook.com/novatecsoftware'});
    this.socials.push({class:'fa fa-twitter', url: 'https://twitter.com/novatecsoftware'});
    this.socials.push({class:'fa fa-linkedin', url: 'https://www.linkedin.com/company/novatec-software-development'});
  }

}
