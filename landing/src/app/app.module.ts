import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AgmCoreModule } from '@agm/core';

import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { OurValuesComponent } from './our-values/our-values.component';
import { ContactComponent } from './contact/contact.component';
import { SocialComponent } from './social/social.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    OurValuesComponent,
    ContactComponent,
    SocialComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCSUS4BJIXt9grPMViJNpDeoRqD27ggnaI'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
