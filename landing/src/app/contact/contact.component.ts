import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {
  
  lat: number = 37.1773832;
  lng: number = -3.6004525;
  constructor() { }

  ngOnInit() {

  }

}
